//
//  SecondViewController.swift
//  NewGalleryProject
//
//  Created by Admin on 10/25/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    
    @IBOutlet var navItem: UINavigationItem!
    @IBOutlet var cellImageView: UIImageView!
    
    @IBOutlet var indicator: UIActivityIndicatorView!
    var model : ImageViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.hidesWhenStopped = true
        if model!.image != nil {
        
            cellImageView.image = model!.image
            
        // Do any additional setup after loading the view. 
        } else {
            indicator.startAnimating()
            downloadImageAsync(url: model!.urlImage as! URL, imageView: cellImageView)
        }
        
        cellImageView.contentMode = .scaleAspectFit
    }

    func downloadImageAsync(url : URL, imageView : UIImageView){
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
            DispatchQueue.main.async {
                let img = UIImage(data: data!)
                imageView.image = img
                self.indicator.stopAnimating()
                
                self.model!.image = img
            }
        }
    }

}
