//
//  ViewController.swift
//  NewGalleryProject
//
//  Created by Admin on 10/25/16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = hexString.substring(from: start)
            
            if hexColor.characters.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}


class TableViewController: UITableViewController {
    
        typealias Payload = [String: AnyObject]
        func shegecit(){
            let planetsJSONURL = URL(string: "https://dl.dropboxusercontent.com/s/mi3lk1kotlymg76/planets_data.json")
            do {
                let jsonRawData = try Data(contentsOf: planetsJSONURL!)
                
                let jsonObject = try JSONSerialization.jsonObject(with: jsonRawData) as? Array<Payload>
                
                for item in jsonObject! {
                    guard let planet = item["planet"] as? Payload,
                        let _ = planet["name"] as? String,
                        let planetThumbnailURL = planet["thumbnailURL"] as? String,
                        let planetPhotoURL = planet["photoURL"] as? String else { return }
                    
                    arrOfUrls.append(URL(string: planetThumbnailURL)!)
                    arrOfUrls.append(URL(string: planetPhotoURL)!)
                    print("planet little: " , planetThumbnailURL)
                    print("planet big: ", planetPhotoURL)
                    
                }
                
            } catch {
            }
        }
    

    
   

    var arrCellData = [ImageViewModel]()
    
    var arrOfUrls = [URL]()

    func countAverageColor(image : UIImage)-> String{
        
        return ""
    }
    
    func downloadAndEditThumbImage(thumbUrl: URL, imageUrl:URL) -> ImageViewModel? {
            var toRet: ImageViewModel = ImageViewModel()
            if let data = NSData(contentsOf: thumbUrl) {
                let thumbImage = UIImage(data: data as Data)
                print("download finished")
                toRet = ImageViewModel( thumbImage: thumbImage!, urlImage: imageUrl as NSURL, urlThumbImage: thumbUrl as NSURL)
            } else { print("error blia")}
        return toRet
        
    }
    func sortSetByColor(){
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shegecit()
        print(arrOfUrls)
        
        var i = 0
        while(i != arrOfUrls.count){
            print("URL= ", arrOfUrls[i])
            arrCellData.append( downloadAndEditThumbImage(thumbUrl: arrOfUrls[i], imageUrl: arrOfUrls[i+1])!)
            i+=2
        }
        sortSetByColor()
        
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("TableViewCell1", owner: self, options: nil)?.first as! TableViewCell1
        cell.imgView.image = arrCellData[indexPath.row].thumbImage
        cell.color.backgroundColor = UIColor(hexString:  arrCellData[indexPath.row].avgColor!)
        cell.avgColor.text = arrCellData[indexPath.row].avgColor
        cell.type.text = arrCellData[indexPath.row].ext
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 109
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "CellSegue", sender: WrapperModel (cell: tableView.cellForRow(at: indexPath) as! TableViewCell1, index: String(describing: indexPath[1] + 1)))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let guest = segue.destination as! SecondViewController
        
        if let cellSender = sender as? WrapperModel{
            guest.navItem.title = "Photo N. " + "" + cellSender.index
            guest.model = arrCellData[Int(cellSender.index)!-1]
            
        } else {
        }
    }
    
    

}

