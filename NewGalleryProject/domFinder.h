//
//  domFinder.h
//  NewGalleryProject
//
//  Created by Admin on 10/26/16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import &lt; UIKit/UIKit.h&gt;

@interface UIImage (AverageColor)
- (UIColor *)averageColor;
@end

